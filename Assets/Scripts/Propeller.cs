﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propeller : MonoBehaviour {

	public Sprite fire01;
	public Sprite fire03;
	private SpriteRenderer sr;


	// Use this for initialization
	void Awake () {
		sr = GetComponent<SpriteRenderer> ();
		Stop ();
	}
	
	public void BlueFire(){
		sr.enabled = true;
		sr.sprite = fire01;
	}

	public void RedFire(){
		sr.enabled = true;
		sr.sprite = fire03;
	}

	public void Stop(){
		sr.enabled = false;
	}
}
