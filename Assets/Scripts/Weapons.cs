﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    
public class Weapons : MonoBehaviour
{
    private Cartridge cart;
    public Cannon cannon;
    public GameObject bullet;
    public int maxBullets;
    public Transform bulletPosition;
    public void ShotWeapons()
    {
        cannon.ShotCannon(cart.GetBullet(),transform.position,0f);

    }
    private void Start()
    {
        cart = new Cartridge(bullet, maxBullets, bulletPosition);
    }
}
